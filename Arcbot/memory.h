#ifndef MEMORY_H
#define MEMORY_H


#include <QMainWindow>
#include <QDebug>
#include <string>
#include <Windows.h>
#include <TlHelp32.h>
#include <stdint.h>
#include <vector>
#include <map>

class Memory
{
    friend class Control;
public:
    Memory();
    Memory(std::string _ProcName);
    Memory(unsigned int _PID);
    ~Memory();

    inline bool IsOpened();
    HANDLE GetModuleHandle(std::string _ModuleName);

    int Read(unsigned int _Address, void *ptr, size_t size);

    template<typename T>
    int Read(unsigned int _Address, T *t)
    {
        return ReadProcessMemory(m_Handle, (LPCVOID)_Address, t, sizeof(T), 0);
    }

    template<typename T>
    T Read(unsigned int _Address)
    {
        T temp;
        ReadProcessMemory(m_Handle, (LPCVOID)_Address, &temp, sizeof(T), 0);
        return temp;
    }

private:
    void Open();

public:
    unsigned int m_ProcessBaseAddress;

private:
    HANDLE m_Handle;
    unsigned int m_PID;
};

#endif // Memory_H
