#include "memory.h"
#include <iostream>

Memory::Memory() : m_PID(0), m_Handle(INVALID_HANDLE_VALUE) {}

Memory::~Memory()
{
    if (this->IsOpened())
        CloseHandle(m_Handle); //RAII
}

Memory::Memory(std::string _ProcName) : Memory()
{
    PROCESSENTRY32 entry;
    entry.dwSize = sizeof(PROCESSENTRY32);
    HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
    if (Process32First(snapshot, &entry) == TRUE){
        do
        {
          QString _Proc = QString::fromWCharArray(entry.szExeFile);
          if (_ProcName.compare(_Proc.toStdString()) == 0)
                m_PID = entry.th32ProcessID;

        } while (Process32Next(snapshot, &entry) == TRUE);
    }
    if (m_PID)
    {
        this->Open();
        m_ProcessBaseAddress = (unsigned int)this->GetModuleHandle(_ProcName);
    }
}

Memory::Memory(unsigned int _PID) : m_PID(_PID)
{
    if (m_PID)
    {
        PROCESSENTRY32 entry;
        entry.dwSize = sizeof(PROCESSENTRY32);
        HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
        m_PID = _PID;
        do
        {

                if (m_PID == entry.th32ProcessID)
                {
                    std::string modulename = QString::fromStdWString(entry.szExeFile).toStdString();
                    m_ProcessBaseAddress = (unsigned int)this->GetModuleHandle(modulename);

                }
        } while (Process32Next(snapshot, &entry) == TRUE);
        this->Open();
    }
}

HANDLE Memory::GetModuleHandle(std::string _ModuleName)
{
    if (m_PID == 0)
        return INVALID_HANDLE_VALUE;

    HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, m_PID);
    if (hSnapshot != INVALID_HANDLE_VALUE)
    {
        MODULEENTRY32 ModuleEntry32;
        ModuleEntry32.dwSize = sizeof(MODULEENTRY32);

        if (Module32First(hSnapshot, &ModuleEntry32))
        {
            do
            {
               QString _Module = QString::fromWCharArray(ModuleEntry32.szModule);
                if (_ModuleName.compare(_Module.toStdString()) == 0)
                {
                    CloseHandle(hSnapshot);
                    return ModuleEntry32.hModule;
                }
            } while (Module32Next(hSnapshot, &ModuleEntry32));
        }
        CloseHandle(hSnapshot);
    }
    return INVALID_HANDLE_VALUE;
}

bool Memory::IsOpened()
{
    return m_Handle != INVALID_HANDLE_VALUE;
}

void Memory::Open()
{
    m_Handle = OpenProcess(PROCESS_VM_READ, false, m_PID);
}

int Memory::Read(unsigned int _Address, void *ptr, size_t size)
{
    return ReadProcessMemory(m_Handle, (LPCVOID)_Address, ptr, size, 0);
}
