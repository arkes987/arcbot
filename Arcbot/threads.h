#ifndef THREADS_H
#define THREADS_H
#include <QThread>
#include "functions.h"

class MainThread : public QThread
{
public:
    MainThread(Functions &_Func) : func(_Func) {}
    void run();


    Functions &func;
};



#endif // THREADS_H
