#-------------------------------------------------
#
# Project created by QtCreator 2015-03-09T17:36:07
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Arcbot
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    memory.cpp \
    structures.cpp \
    functions.cpp \
    client.cpp \
    menu.cpp \
    control.cpp \
    threads.cpp \
    startup.cpp

HEADERS  += mainwindow.h \
    consts.h \
    memory.h \
    structures.h \
    functions.h \
    client.h \
    menu.h \
    control.h \
    threads.h \
    startup.h

FORMS    += mainwindow.ui \
    menu.ui
