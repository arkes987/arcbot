#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include <QDebug>
#include <stdint.h>
#include "memory.h"
#include "consts.h"
#include "structures.h"
#include <QElapsedTimer>

class Functions
{
public:
    Functions(Memory &_Mem) : m_Mem(_Mem){}
    void updateValues();
    std::string getName();
    Structures::BattleList getCreatures();
    Structures::Monster getLocal();


private:
    Structures::Monster localCre;
    Structures::BattleList getCre;
    uint16_t id;
    bool isConnected;
    std::string name;
    uint32_t health;
    uint32_t mana;
    uint32_t healthMax;
    uint32_t manaMax;
    uint32_t healthPc;
    uint32_t manaPc;
    uint32_t x;
    uint32_t y;
    uint16_t z;

private:
    Memory &m_Mem;
    Structures::BattleList m_Battle;
};

#endif // FUNCTIONS_H
