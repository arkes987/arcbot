#ifndef MENU_H
#define MENU_H

#include <QWidget>
#include <QDebug>
#include "structures.h"
#include "functions.h"
#include "memory.h"
#include "client.h"

namespace Ui {
class menu;
}

class menu : public QWidget
{
    Q_OBJECT

public:
    explicit menu(QWidget *parent = 0);
    ~menu();

private slots:

private:
    Ui::menu *ui;
};

#endif // MENU_H
