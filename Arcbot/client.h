#ifndef CLIENT_H
#define CLIENT_H
#include "memory.h"
#include "functions.h"


class Client
{
public:

   // Memory::Memory(unsigned int _PID) : m_PID(_PID)

    Client(unsigned int _PID) : memory(Memory(_PID)), func(Functions(memory)){}

    std::string getname();
private:
    Memory memory;
    Functions func;
};

#endif // CLIENT_H
