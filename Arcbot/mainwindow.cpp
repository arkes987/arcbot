#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow){
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    inject();
}

void MainWindow::on_pushButton_2_clicked()
{
    refresh();
}

void MainWindow::refresh()
{
    PROCESSENTRY32 entry;
    entry.dwSize = sizeof(PROCESSENTRY32);
    HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
    ui->listWidget->clear();
    do
    {
        if (QString::fromWCharArray(entry.szExeFile) == "Tibia.exe")
        {
            unsigned int Processid = entry.th32ProcessID;
            Client c(Processid);
            std::string name  = c.getname();
            if(name.length() == 0)
            {
                continue;
            }
            ui->listWidget->addItem(QString::number(entry.th32ProcessID) + " ->" + name.c_str());
        }
    } while (Process32Next(snapshot, &entry) == TRUE); 
}
void MainWindow::inject()
{
    unsigned int ProcessId = 0;
    if (ui->listWidget->selectedItems().count() > 0)
    {
        int pos = 0;
        QString item = ui->listWidget->selectedItems()[0]->text();
        QRegExp rx("(\\d+)");

        while (pos = rx.indexIn(item, pos) != -1)
        {
            ProcessId = rx.cap(1).toInt();
            pos += rx.matchedLength();
        }
    }
    if (ProcessId != 0)
    {
        Startup *run = new Startup(ProcessId);
        menu *mMyNewWindow = new menu();
        mMyNewWindow->show();
        this->hide();
    }
}
