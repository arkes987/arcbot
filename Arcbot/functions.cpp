#include "functions.h"


std::string Functions::getName()
{
    return name;
}

Structures::BattleList Functions::getCreatures()
{
    return getCre;
}

Structures::Monster Functions::getLocal()
{
    return localCre;
}


void Functions::updateValues()
{
    id =  m_Mem.Read<uint16_t>(m_Mem.m_ProcessBaseAddress + PLAYER_ID_OFFSET);
    uint32_t status = m_Mem.Read<uint32_t>(m_Mem.m_ProcessBaseAddress + CLIENT_STATUS_OFFSET);
    uint32_t hp =  m_Mem.Read<uint32_t>(m_Mem.m_ProcessBaseAddress + PLAYER_HP_OFFSET);
    uint32_t hpmax = m_Mem.Read<uint32_t>(m_Mem.m_ProcessBaseAddress + PLAYER_HPMAX_OFFSET);
    uint32_t mp =  m_Mem.Read<uint32_t>(m_Mem.m_ProcessBaseAddress + PLAYER_MP_OFFSET);
    uint32_t mpmax =  m_Mem.Read<uint32_t>(m_Mem.m_ProcessBaseAddress + PLAYER_MPMAX_OFFSET);
    uint32_t xor = m_Mem.Read<uint32_t>(m_Mem.m_ProcessBaseAddress + CLIENT_XOR_OFFSET);
    m_Mem.Read(m_Mem.m_ProcessBaseAddress + BATTLELIST_OFFSET, &m_Battle.List[0], sizeof(Structures::Monster) * 250);
    m_Battle.IdList.clear();
    for (Structures::Monster &c : m_Battle.List)
    {
        if (c.DistanceID != 0)
        {
            m_Battle.IdList[c.DistanceID] = &c;
        }
    }
    if(m_Battle.IdList.count(id) == 0)
    {
        return;
    }
    localCre = *m_Battle.IdList[this->id];
    getCre = m_Battle;
    name = localCre.DistanceName;
    x = localCre.DistanceX;
    y = localCre.DistanceY;
    z = localCre.DistanceZ;

    isConnected = (status == 11) ? true : false;
    health = hp ^ xor;
    mana = mp ^ xor;
    healthMax = hpmax ^ xor;
    manaMax = mpmax ^ xor;
    healthPc = (int)((double)health/(double)healthMax * 100);
    manaPc = (int)((double)mana/(double)manaMax * 100);

}


