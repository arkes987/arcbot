#ifndef STRUCTURES_H
#define STRUCTURES_H

#include <string>
#include <Windows.h>
#include <TlHelp32.h>
#include <stdint.h>
#include <vector>
#include <map>


class Structures
{
public:
    class Monster
    {
    public:
        uint16_t DistanceID; //0x0000
        uint16_t DistanceType; //0x0002
        char DistanceName[24]; //0x0004
        int8_t _0x001C[8];
        int32_t DistanceZ; //0x002C
        int32_t DistanceY; //0x0028
        int32_t DistanceX; //0x0024
        int32_t DistanceScreenOffsetHoriz; //0x0030
        int32_t DistanceScreenOffsetVer; //0x0034
        int8_t _0x0038[20];
        int32_t DistanceIsWalking; //0x004C
        int32_t DistanceDirection; //0x0050
        int8_t _0x0054[8];
        int32_t DistanceOutfit; //0x005C
        int32_t DistanceColorHead; //0x0060
        int32_t DistanceColorBody; //0x0064
        int32_t DistanceColorLegs; //0x0068
        int32_t DistanceColorFeet; //0x006C
        int32_t DistanceAddon; //0x0070
        int32_t DistanceLight; //0x0074
        int32_t DistanceLightColor; //0x0078
        int8_t _0x007C[8];
        int32_t DistanceBlackSquare; //0x0084
        int32_t DistanceHPBar; //0x0088
        int32_t DistanceWalkSpeed; //0x008C
        int32_t DistanceIsVisible; //0x0090
        int32_t DistanceSkull; //0x0094
        int32_t DistanceParty; //0x0098
        int8_t _0x009C[4];
        int32_t DistanceWarIcon; //0x00A0
        int32_t DistanceIsBlocking; //0x00A4
        int8_t _0x00ad9C[52];
    };


    class BattleList
    {
    public:
        BattleList() : List(1300) {}
        std::vector<Monster> List;
        std::map<int16_t, Monster*> IdList; //mapa id -> &List[]
    };

    struct Vips
    {
    public:
        uint32_t DistanceID; // 4BYTE
        char DistanceName[24]; // 24BYTE
        char _0x0038[10]; //10BYTE
        int8_t DistanceStatus;
        int32_t DistanceIcon;
    };
};
#endif // STRUCTURES_H
