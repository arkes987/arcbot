![logo.png](https://bitbucket.org/repo/8Kro8o/images/2920577561-logo.png)

### Arcbot ###

Arcbot is opensource C++ code created with QtCreator. It's software created for fun in free time.

**Main assumptions**

* Multi client
* Healer
* Targeting
* Looter
* Cavebot
* Scripter in Lua

The main target is to create pretty efficient application which is easy to use.
If you have questions or want to help please write on my email.


Thanks to Vesim, szulak and other guys from irc chanel.

**Contact: arkes987@gmail.com**